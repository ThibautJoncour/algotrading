# -*- coding: utf-8 -*-
"""
Created on Fri Jun  3 14:39:34 2022

@author: Joncour
"""


#MAM TRANSITION DURABLE OBLIGATIONS
import matplotlib.pyplot as plt
import pandas as pd
plt.style.use('ggplot')
import seaborn as sns
import math
#MAM SHORT TERM BONDS ESG
#MAM HIGH YIELD ESG
#MAM FLEXIBLE BONDS ESG
#MAM OBLI CONVERTIBLES ESG
import yfinance as yf
import numpy as np



ticker = [['GFI', 'BMRRY', 'CVNA', 'LAC', 'RLLCF', 'S', 'NTRA', 'BMRPF', 'AMR', 'TKGBY', 'SNAP', 'GTLB', 'PODD', 'ARCH', 'MRVI', 'GME', 'UDMY', 'NTCT', 'CHWY', 'HMY', 'QDEL']]
#df = df1.pct_change().dropna()*100



list_ticker = [i for x in ticker for i in x]
len(list_ticker)

def Vol(ticker):
    
    data = yf.download(tickers=ticker,period='10mo', interval='1wk')
    data['Ret_Close'] = data['Close'].pct_change().fillna(method='ffill').dropna()*100
    data['Volatility'] = data['Ret_Close'].rolling(window=2).std()*100
    data['ret_vol'] = data['Volatility'].pct_change()*100
    data['ticker']=str(ticker)
    

    return data


stack = []

def buy_sell(data) : 
    sig_buy = []
    sig_sell = []
    flag = -1
    stack = []
    for i in range(1,len(data)):
        if data["Volatility"][i] < data["Volatility"][i-1] and data["Volatility"][i-1] < data["Volatility"][i-2] :
            if flag != 1:
                data['Position']='Long' 
                flag = 1
            else :
                sig_buy.append(np.nan)
                sig_sell.append(np.nan)
        elif data["Volatility"][i] > data["Volatility"][i-1] and data["Volatility"][i-1] > data["Volatility"][i-2]:
            if flag != 0:
                data['Position']= 'Short'
                flag = 0
            
     
    
    stack.append((data[['ticker','Position']][-1:]))    
       
    return print(stack)




for i in range(0,len(list_ticker)):
    (buy_sell((Vol(ticker[0][i]))))


